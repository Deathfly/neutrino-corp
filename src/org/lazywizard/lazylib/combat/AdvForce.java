package org.lazywizard.lazylib.combat;

import com.fs.starfarer.api.combat.CombatEntityAPI;
import org.lazywizard.lazylib.VectorUtils;
import org.lwjgl.util.vector.Vector2f;

public class AdvForce {

    /**
     * Apply momentum to an object. 
     *
     * Momentum is multiplied by 100 to avoid requiring ridiculous momentum amounts.
     *
     * @param entity The {@link CombatEntityAPI} to apply the momentum to.
     * @param direction The directional vector of the momentum (this will
     * automatically be normalized).
     * @param pointOfImpact Where the momentum should apply to.
     * @param momentum How much momentum to apply. Unit is how much it takes to modify
     * a 100 weight object's velocity by 1 su/sec.
     */
    public static void applyMomentum(CombatEntityAPI entity, Vector2f pointOfImpact, Vector2f direction, float momentum) {
        // Filter out forces without a direction
        if (direction.lengthSquared() == 0) {
            return;
        }
        // Momentum is far too weak otherwise
        momentum *= 100f;
        // Avoid divide-by-zero errors...
        float mass = Math.max(1f, entity.getMass());
        // Doing some vector calculate
        Vector2f BPtoMC = Vector2f.sub(entity.getLocation(), pointOfImpact, null);
        Vector2f forceV = new Vector2f();
        direction.normalise(forceV);
        forceV.scale(momentum);
        // get force vector
        BPtoMC.normalise(BPtoMC);
        // calculate acceleration
        BPtoMC.scale(Vector2f.dot(forceV, BPtoMC) / mass);
        // Apply velocity change
        Vector2f.add(BPtoMC, entity.getVelocity(), entity.getVelocity());
        // calculate moment change 
        float angularAcc = VectorUtils.getCrossProduct(forceV, BPtoMC) / (0.5f * mass * entity.getCollisionRadius() * entity.getCollisionRadius());
        angularAcc = (float) Math.toDegrees(angularAcc);
        // Apply angular velocity change
        entity.setAngularVelocity(angularAcc + entity.getAngularVelocity());
    }
}
