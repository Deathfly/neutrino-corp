package org.lazywizard.lazylib.combat.entities;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lwjgl.util.vector.Vector2f;

/**
 * An implementation of {@code CombatEntityAPI} that follows and rotates with
 * another anchoring {@code CombatEntityAPI} and moving relatively. (All for the
 * beams! oh, I used to love beams until I hate them)
 *
 * @author Deathfly
 */
public class AnchoredMovingEntity extends EntityBase {

    protected Vector2f relativeVelocity, location, relativeLocation;
    protected CombatEntityAPI anchor;
    protected float timeStamp, relativeDistance, relativeAngle, angle;
    protected float TET = Global.getCombatEngine().getTotalElapsedTime(false);

    public AnchoredMovingEntity(CombatEntityAPI anchor, Vector2f location, Vector2f relativeVelocity) {
        reanchor(anchor, location, relativeVelocity);
    }

    public void reanchor(CombatEntityAPI newAnchor, Vector2f newLocation, Vector2f newRelativeVelocity) {
        relativeDistance = MathUtils.getDistance(newAnchor.getLocation(), newLocation);
        angle = newAnchor.getFacing();
        relativeAngle = MathUtils.clampAngle(VectorUtils.getAngle(newAnchor.getLocation(), newLocation) - newAnchor.getFacing());
        anchor = newAnchor;
        this.relativeVelocity = newRelativeVelocity;
        timeStamp = TET;
    }

    public void rotateRelativeVelocity() {
        if (anchor.getFacing() != angle) {
            VectorUtils.rotate(relativeVelocity, MathUtils.getShortestRotation(angle, anchor.getFacing()), relativeVelocity);
            angle = anchor.getFacing();
        }
    }

    public void setLocation(Vector2f location) {
        reanchor(this.anchor, location, this.relativeVelocity);
    }

    @Override
    public Vector2f getLocation() {
        Vector2f toReturn = new Vector2f(anchor.getLocation());
        if (timeStamp != TET) {
            rotateRelativeVelocity();
            Vector2f variation = new Vector2f(relativeVelocity);
            variation.scale(TET - timeStamp);
            Vector2f.add(toReturn, variation, toReturn);
        }
        return toReturn;
    }

    public void setVelocity(Vector2f velocity) {
        reanchor(this.anchor, this.location, velocity);
    }

    @Override
    public Vector2f getVelocity() {
        rotateRelativeVelocity();
        return Vector2f.add(anchor.getVelocity(), relativeVelocity, null);
    }
}
