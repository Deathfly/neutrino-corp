package org.lazywizard.lazylib.combat.entities;

import com.fs.starfarer.api.Global;
import org.lwjgl.util.vector.Vector2f;

/**
 * An implementation of {@code CombatEntityAPI} that moving.
 *
 * @author Deathfly
 */
public class MovingEntity extends EntityBase {

    protected Vector2f velocity, location;
    protected float timeStamp = 0;
    protected float TET = Global.getCombatEngine().getTotalElapsedTime(false);

    public MovingEntity(Vector2f location, Vector2f velocity) {
        this.location = location;
        this.velocity = velocity;
        timeStamp = TET;
    }

    public void setLocation(Vector2f location) {
        this.location = location;
        this.timeStamp = TET;
    }

    @Override
    public Vector2f getLocation() {
        if (velocity != null && velocity != new Vector2f(0, 0) && TET != timeStamp) {
            Vector2f variation = new Vector2f(velocity);
            variation.scale(TET - timeStamp);
            Vector2f.add(location, variation, location);
            timeStamp = TET;
        }
        return location;
    }

    public void setVelocity(Vector2f velocity) {
        if (timeStamp != 0 && TET != timeStamp) {
            getLocation();
        }
        this.velocity = velocity;
    }

    @Override
    public Vector2f getVelocity() {
        return velocity;
    }
}
