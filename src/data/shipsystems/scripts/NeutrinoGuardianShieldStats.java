// by Deathfly
package data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.FluxTrackerAPI;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;
import data.scripts.plugins.Neutrino_LocalData;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class NeutrinoGuardianShieldStats extends BaseShipSystemScript {

    private static final String KEY = "Neutrino_LocalData";

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        stats.getZeroFluxMinimumFluxLevel().modifyMult(id, 0f);
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        stats.getZeroFluxMinimumFluxLevel().unmodify();
    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        return null;
    }

    @Override
    public String getInfoText(ShipSystemAPI system, ShipAPI ship) {

        CombatEngineAPI engine = Global.getCombatEngine();
        if (engine == null || ship != engine.getPlayerShip()) {
            return null;
        }
        final Neutrino_LocalData.LocalData localData = (Neutrino_LocalData.LocalData) engine.getCustomData().get(KEY);
        if (localData.guardianShieldMap.containsKey(ship)) {
            ShipAPI drone = localData.guardianShieldMap.get(ship);
            if (drone.isAlive()) {
                FluxTrackerAPI shieldFlux = drone.getFluxTracker();
                if (shieldFlux.isOverloaded()) {
                    return "Warning! Shield Core Overloaded!";
                } else {
                    String state;
                    if (system.isOn()) {
                        state = "Actived: Flux Capacity At ";
                    } else {
                        state = "Standby: Flux Capacity At ";
                    }
                    float fluxLevel = shieldFlux.getFluxLevel();
                    fluxLevel *= 100f;
                    BigDecimal b = new BigDecimal(fluxLevel);
                    String fluxLevelS = b.setScale(2, RoundingMode.HALF_UP).toString();
                    String data = state + fluxLevelS + "%";
                    return data;
                }
            } else {
                return "Warning! Shield Core Ejected!";
            }
        }
        return null;
    }

    @Override
    public boolean isUsable(ShipSystemAPI system, ShipAPI ship) {
        CombatEngineAPI engine = Global.getCombatEngine();
        if (engine == null || ship != engine.getPlayerShip()) {
            return false;
        }
        final Neutrino_LocalData.LocalData localData = (Neutrino_LocalData.LocalData) engine.getCustomData().get(KEY);
        if (localData.guardianShieldMap.containsKey(ship)) {
            ShipAPI drone = localData.guardianShieldMap.get(ship);
            return drone != null && drone.isAlive();
        }
        return false;
    }
}
