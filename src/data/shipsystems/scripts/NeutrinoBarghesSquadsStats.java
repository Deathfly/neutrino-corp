// By Deathfly
package data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.DroneLauncherShipSystemAPI;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;
import com.fs.starfarer.combat.ai.M;
import com.fs.starfarer.combat.entities.Ship;
import com.fs.starfarer.combat.systems.N;
import java.awt.Color;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

public class NeutrinoBarghesSquadsStats implements ShipSystemStatsScript {

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        ShipAPI ship;
        if (stats.getEntity() instanceof ShipAPI) {
            ship = (ShipAPI) stats.getEntity();
        } else {
            return;
        }
        if (((N) ship.getSystem()).getDroneOrders() == DroneLauncherShipSystemAPI.DroneOrders.RECALL) {
            ((N) ship.getSystem()).setDeploy();
        }
        for (ShipAPI drone : ship.getDeployedDrones()) {
//            drone.setOriginalOwner(ship.getOwner());
            drone.setOwner(ship.getOwner());
            if (ship.isAlly()) {
                Global.getCombatEngine().getFleetManager(ship.getOwner()).getDeployedFleetMember(drone).getMember().setAlly(true);
            }
            drone.setCurrentCR(ship.getCurrentCR());
            if (((Ship) drone).getSinceLaunch() == 0) {
                drone.getLocation().set(MathUtils.getRandomPointInCircle(ship.getLocation(), 500f));
                ((Ship) drone).setColor(new Color(255, 255, 255, 0));
                ((Ship) drone).fadeToColor(3, Color.WHITE);
                drone.setJitter(this, Color.BLUE, 1, 50, 5);
            }
        }
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        return null;
    }
}
