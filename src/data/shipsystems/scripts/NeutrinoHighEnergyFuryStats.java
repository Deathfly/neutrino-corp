package data.shipsystems.scripts;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;

public class NeutrinoHighEnergyFuryStats extends BaseShipSystemScript {

    public static final float DAMAGE_BONUS_PERCENT = 25f;
    public static final float ROF_BONUS_PERCENT = 25f;
    public static final float FLUX_REDUCTION = 15f;
//    public static final float EXTRA_DAMAGE_TAKEN_PERCENT = 100f;

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {

        float damageBonusPercent = DAMAGE_BONUS_PERCENT * effectLevel;
        stats.getEnergyWeaponDamageMult().modifyPercent(id, damageBonusPercent);
        float ROFBonusPercent = ROF_BONUS_PERCENT * effectLevel;
        stats.getEnergyRoFMult().modifyPercent(id, ROFBonusPercent);
        float fluxReduction = -FLUX_REDUCTION * effectLevel;
        stats.getEnergyWeaponFluxCostMod().modifyPercent(id, fluxReduction);
//        float damageTakenPercent = EXTRA_DAMAGE_TAKEN_PERCENT * effectLevel;
//        stats.getShieldAbsorptionMult().modifyPercent(id, damageTakenPercent);
        if (state == State.OUT) {
            stats.getZeroFluxMinimumFluxLevel().unmodify(id);
        } else {
            stats.getZeroFluxMinimumFluxLevel().modifyFlat(id, 1);
        }
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        stats.getEnergyWeaponDamageMult().unmodify(id);
        stats.getEnergyRoFMult().unmodify(id);
        stats.getEnergyWeaponFluxCostMod().unmodify(id);
        stats.getShieldDamageTakenMult().unmodify(id);
        stats.getZeroFluxMinimumFluxLevel().unmodify(id);
    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        float damageBonusPercent = DAMAGE_BONUS_PERCENT * effectLevel;
        float ROFBonusPercent = ROF_BONUS_PERCENT * effectLevel;
        float fluxReduction = -FLUX_REDUCTION * effectLevel;
//        float damageTakenPercent = EXTRA_DAMAGE_TAKEN_PERCENT * effectLevel;
        if (index == 0) {
            return new StatusData("+" + (int) damageBonusPercent + "% energy weapon damage and " + (int) ROFBonusPercent + "% rate of fire", false);
        } else if (index == 1) {
            return new StatusData("" + (int) fluxReduction + "% energy weapon flux cost", false);
        } else if (index == 2) {
            return new StatusData("enable engine boost", false);
        }
        return null;
    }
}
