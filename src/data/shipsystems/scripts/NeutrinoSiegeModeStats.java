package data.shipsystems.scripts;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;

public class NeutrinoSiegeModeStats extends BaseShipSystemScript {

    // Credit to Deathfly for Fix when only armored weapon mounts is installed.
    public static final float DAMAGE_BONUS_PERCENT = 50F;
    public static final float ROF_PENALTY_PERCENT = -40f;
    public static final float WEAPON_RANGE_BONUS_MULT = 2F;//so the final weapon range should be 100% + 300%. Stack with skill and hull mod.
    public static final float TURN_PENALTY_MULT = 0.95F; //means the weapon turn rate will be 1-95%=5%. Stack with hull mod.
    public static final float PROJECTILE_SPEED_BONUS_PERCENT = 25F;

    public static final float BEAM_RANGE_PENALTY_MULT = 1F;//OK, beam is OP on this one. try to make them only got 33.3% extra Range.
    public static final float BEAM_TURN_PENALTY = 0.7F;//And lease penalty.

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        float effectLevelSquare = effectLevel * effectLevel;
        if (state == State.OUT) {
            stats.getMaxSpeed().unmodify(id);
            stats.getMaxTurnRate().unmodify(id);
        } else {
            stats.getMaxSpeed().modifyFlat(id, -200F);
            stats.getAcceleration().modifyPercent(id, 100F * effectLevel);
            stats.getDeceleration().modifyPercent(id, 100F * effectLevel);
            stats.getTurnAcceleration().modifyPercent(id, 0.0F);
            stats.getMaxTurnRate().modifyPercent(id, 0.0F);
        }
        if (state == State.ACTIVE) {
            stats.getEnergyRoFMult().unmodifyMult(id);
            stats.getEnergyRoFMult().modifyPercent(id, ROF_PENALTY_PERCENT);
        } else {
            stats.getEnergyRoFMult().modifyMult(id, 0);
        }
        float damageBonusPercent = DAMAGE_BONUS_PERCENT * effectLevelSquare;
        stats.getEnergyWeaponDamageMult().modifyPercent(id, damageBonusPercent);

        float weaponRangePercent = 1F + WEAPON_RANGE_BONUS_MULT * effectLevel;
        stats.getEnergyWeaponRangeBonus().modifyMult(id, weaponRangePercent);
        float turnMult = 1F - TURN_PENALTY_MULT * effectLevelSquare;
        stats.getWeaponTurnRateBonus().modifyMult(id, turnMult);
        float projectileSpeedBonus = PROJECTILE_SPEED_BONUS_PERCENT * effectLevelSquare;
        stats.getProjectileSpeedMult().modifyPercent(id, projectileSpeedBonus);
        //neaf for beams.
        float beamRangePenalty = 1F / (1F + BEAM_RANGE_PENALTY_MULT * effectLevelSquare);
        stats.getBeamWeaponRangeBonus().modifyMult(id, beamRangePenalty);
        float beamTurnMult = 1F - BEAM_TURN_PENALTY * effectLevelSquare;
        stats.getBeamWeaponTurnRateBonus().modifyMult(id, beamTurnMult);
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        stats.getEnergyWeaponDamageMult().unmodify(id);
        stats.getEnergyRoFMult().unmodify(id);
        stats.getEnergyWeaponRangeBonus().unmodify(id);
        stats.getBeamWeaponRangeBonus().unmodify(id);
        stats.getWeaponTurnRateBonus().unmodify(id);
        stats.getBeamWeaponTurnRateBonus().unmodify(id);
        stats.getProjectileSpeedMult().unmodify(id);
        stats.getMaxSpeed().unmodify(id);
        stats.getMaxTurnRate().unmodify(id);
        stats.getTurnAcceleration().unmodify(id);
        stats.getAcceleration().unmodify(id);
        stats.getDeceleration().unmodify(id);

    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (index == 0) {
            return new StatusData("+" + (int) (DAMAGE_BONUS_PERCENT * effectLevel) + "% energy weapon damage", false);
        } else if (index == 1) {
            if (state == State.ACTIVE) {
                return new StatusData((int) (ROF_PENALTY_PERCENT * effectLevel) + "% energy weapon rate of fire", true);
            } else {
                return new StatusData("re-routing weapon energy distribution", true);
            }
        } else if (index == 2) {
            return new StatusData("+" + (int) (effectLevel * 200) + "% energy projectile weapons range and " + (int) (PROJECTILE_SPEED_BONUS_PERCENT * effectLevel) + "% projectile speed", false);
        } else if (index == 3) {
            return new StatusData("+" + (int) (effectLevel * 50) + "% energy beam weapons range", false);
        } else if (index == 4) {
            return new StatusData("weapon turn speed greatly slowed", true);
        }
        return null;
    }
}
