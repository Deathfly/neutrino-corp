package data.shipsystems.scripts;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;
import com.fs.starfarer.api.util.Misc;
import java.awt.Color;
import org.lwjgl.util.vector.Vector2f;

public class NeutrinoReactionControlStats extends BaseShipSystemScript {

    Color colorFrom = new Color(120, 165, 200, 255);
    Color colorTo = new Color(235, 113, 113, 185);

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {

        if (state == ShipSystemStatsScript.State.OUT) {
            stats.getMaxSpeed().modifyPercent(id, 10f * effectLevel); // to slow down ship to its regular top speed while powering drive down
            stats.getMaxTurnRate().modifyPercent(id, 100f * effectLevel);
            stats.getDeceleration().modifyPercent(id, 100f * effectLevel);
        } else {
            stats.getMaxSpeed().modifyFlat(id, 400f * effectLevel);
            stats.getAcceleration().modifyFlat(id, 1800f * effectLevel);
            stats.getDeceleration().modifyFlat(id, 400f * effectLevel);
            stats.getTurnAcceleration().modifyFlat(id, 90f * effectLevel);
            stats.getTurnAcceleration().modifyPercent(id, 200f * effectLevel);
            stats.getMaxTurnRate().modifyFlat(id, 40f * effectLevel);
            stats.getMaxTurnRate().modifyPercent(id, 100f * effectLevel);
        }
        //VFX
        ShipAPI ship = null;
        if (stats.getEntity() instanceof ShipAPI) {
            ship = (ShipAPI) stats.getEntity();
        }
        if (ship != null) {
            Color jetColor = Misc.interpolateColor(colorFrom, colorTo, effectLevel);
            ship.setJitter(this, jetColor, effectLevel * 2, 8, 5);
            ship.setJitterUnder(this, jetColor, effectLevel * 2, 40, 7);
//            int alpha = 100;
//            Color afterimageColor = new Color(jetColor.getRed(), jetColor.getGreen(), jetColor.getBlue(), alpha);
//            float duration = effectLevel * 0.3f;
////            duration *= duration;
//            Vector2f vel = ship.getVelocity();
//            float length = -0.2f;
//
////            ship.addAfterimage(Misc.interpolateColor(colorFrom, colorTo, effectLevel), 0, 0, vel.getX() * length, vel.getY() * length, 3, 0, duration, duration, true, true, true);
//            ship.addAfterimage(afterimageColor, 0, 0, vel.getX() * length, vel.getY() * length, 3, 0, duration, duration, true, false, false);
        }
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        stats.getMaxSpeed().unmodify(id);
        stats.getMaxTurnRate().unmodify(id);
        stats.getTurnAcceleration().unmodify(id);
        stats.getAcceleration().unmodify(id);
        stats.getDeceleration().unmodify(id);
    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (index == 0) {
            return new StatusData("improved maneuverability", false);
        } else if (index == 1) {
            return new StatusData("increased top speed", false);
        }
        return null;
    }
}
