//by Deathfly
package data.hullmods;

import com.fs.starfarer.api.combat.ShieldAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import org.lazywizard.lazylib.FastTrig;
import org.lazywizard.lazylib.MathUtils;

public class NeutrinoBroadsideShield extends BaseHullMod {

    private final int ARC_EXPAND_TO = 90;
    @Override
    public void advanceInCombat(ShipAPI ship, float amount) {
        // Refit screen check,
        if (ship.getOriginalOwner() == -1) {
            return;
        }
        ShieldAPI shield = ship.getShield();
        if (shield != null) {
            if (shield.isOn()) {
                float arcToExpand = ARC_EXPAND_TO - ship.getHullSpec().getShieldSpec().getArc();
//                double radians = Math.toRadians(Math.abs(MathUtils.getShortestRotation(ship.getFacing(), ship.getShield().getFacing())));
//                float arc = (float) FastTrig.sin(radians) * arcToExpand + ship.getHullSpec().getShieldSpec().getArc();     
                float deg = Math.abs(MathUtils.getShortestRotation(ship.getFacing(), ship.getShield().getFacing()));
                float arc = deg > 90 ? (165 - deg) / 60 : (deg - 15) / 60;
                arc = Math.max(0, Math.min(1, arc));
                arc *= arcToExpand;
                arc += ship.getHullSpec().getShieldSpec().getArc();
                shield.setArc(arc);
            }
        }
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        // Allows any ship with a Neutrino hull id  
        return ship.getHullSpec().getHullId().startsWith("neutrino_") 
                && ship.getShield()!= null 
                && ship.getShield().getType() == ShieldAPI.ShieldType.OMNI 
                && (ship.getHullSize() == HullSize.CAPITAL_SHIP || ship.getHullSize() == HullSize.CRUISER);
    }

    @Override
    public String getDescriptionParam(int index, HullSize hullSize
    ) {
        if (index == 0) {
            return "" + ARC_EXPAND_TO;
        }
        return null;
    }
}
