package data.hullmods;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;

public class NeutrinoDrone extends BaseHullMod {
    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getCrewLossMult().modifyMult(id, 0f);
        stats.getProjectileSpeedMult().modifyPercent(id, 50);
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return false;
    }
}
