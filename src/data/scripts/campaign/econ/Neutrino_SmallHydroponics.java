package data.scripts.campaign.econ;

import com.fs.starfarer.api.impl.campaign.econ.BaseMarketConditionPlugin;
import static com.fs.starfarer.api.impl.campaign.econ.BaseMarketConditionPlugin.getProductionMult;
import com.fs.starfarer.api.impl.campaign.ids.Commodities;

public class Neutrino_SmallHydroponics extends BaseMarketConditionPlugin {

    @Override
    public void apply(String id) {
        market.getDemand(Commodities.REGULAR_CREW).getDemand().modifyFlat(id, 100);
        market.getDemand(Commodities.REGULAR_CREW).getNonConsumingDemand().modifyFlat(id, 100 * 0.99f);

        float crewDemandMet = market.getDemand(Commodities.REGULAR_CREW).getClampedAverageFractionMet();

        market.getDemand(Commodities.ORGANICS).getDemand().modifyFlat(id, 2500 * crewDemandMet);
        market.getDemand(Commodities.HEAVY_MACHINERY).getDemand().modifyFlat(id, 100 * crewDemandMet);

        float productionMult = getProductionMult(market, Commodities.VOLATILES);

        market.getCommodityData(Commodities.FOOD).getSupply().modifyFlat(id, 5000 * crewDemandMet * productionMult);
    }

    @Override
    public void unapply(String id) {
        market.getDemand(Commodities.REGULAR_CREW).getDemand().unmodify(id);
        market.getDemand(Commodities.REGULAR_CREW).getNonConsumingDemand().unmodify(id);

        market.getDemand(Commodities.ORGANICS).getDemand().unmodify(id);
        market.getDemand(Commodities.HEAVY_MACHINERY).getDemand().unmodify(id);

        market.getCommodityData(Commodities.FOOD).getSupply().unmodify(id);
    }

}
