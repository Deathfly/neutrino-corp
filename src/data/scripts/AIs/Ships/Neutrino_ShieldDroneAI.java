// by Deathfly
// nya, this one comes a little bit...hacky
package data.scripts.AIs.Ships;

import com.fs.starfarer.api.combat.DroneLauncherShipSystemAPI;
import com.fs.starfarer.api.combat.DroneLauncherShipSystemAPI.DroneOrders;
import com.fs.starfarer.api.combat.ShipAIPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.combat.ai.system.drones.DroneAI;
import com.fs.starfarer.combat.entities.Ship;
import com.fs.starfarer.combat.systems.N;

public class Neutrino_ShieldDroneAI extends DroneAI implements ShipAIPlugin {

    ShipAPI drone;
    DroneLauncherShipSystemAPI system;

    public Neutrino_ShieldDroneAI(Ship ship, Ship ship1, N n) {
        super(ship, ship1, n);
        this.drone = (ShipAPI) ship;
        this.system = (DroneLauncherShipSystemAPI) n;
    }

    public Neutrino_ShieldDroneAI(ShipAPI drone, ShipAPI mothership, DroneLauncherShipSystemAPI system) {
        super((Ship) drone, (Ship) mothership, (N) system);
        this.drone = drone;
        this.system = system;
    }

    @Override
    public void advance(float amount) {
        
        super.advance(amount);
    }
//
//    @Override
//    public boolean needsRefit() {
//        return (drone.getFluxTracker().getFluxLevel() > 0.95f
//                || drone.getFluxTracker().isOverloaded()
//                || drone.getHullLevel() < 0.5f
//                || system.getDroneOrders() == DroneOrders.RECALL);
//    }
}
