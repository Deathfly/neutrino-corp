package data.scripts.weapons;

import java.awt.Color;

import org.lwjgl.util.vector.Vector2f;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.OnHitEffectPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipEngineControllerAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import data.hullmods.TEM_LatticeShield;
import data.scripts.NCModPlugin;
import data.scripts.plugins.Neutrino_LocalData.LocalData;
import data.scripts.util.Neutrino_ParticlesEffectLib;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import org.lazywizard.lazylib.CollisionUtils;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.entities.AnchoredEntity;

public class NeutUnstablePhotonEffect implements OnHitEffectPlugin {

    private static final String KEY = "Neutrino_LocalData";
    private Vector2f from, to;
    private int count = 0;
    private int countMult = 1;

    private final static float maxArcRange = 100f;
    private final static boolean bypassShields = true;
    private Color color1 = new Color(227, 40, 26, 150);
    private final static Color color2 = new Color(173, 252, 251, 150);
    private final static Color color3 = new Color(227, 40, 26, 200);
    private final static Color color4 = new Color(173, 252, 251, 200);

    @Override
    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, CombatEngineAPI engine) {
        final LocalData localData = (LocalData) engine.getCustomData().get(KEY);
        final Set<DamagingProjectileAPI> critSet = localData.critSet;
        if (critSet.contains(projectile) && target instanceof ShipAPI) {
            float dam = (projectile.getDamageAmount());
            DamageType dmgType;
//            engine.applyDamage(target, point, projectile.getDamageAmount(), dmgType, 0, false, false, projectile.getSource());
            int mult = Math.round(dam / (projectile.getDamage().getMultiplier() * 50));
            dam /= mult;
            float emp = dam * 0.5f;
            float facing = projectile.getFacing();
            boolean shieldHitCheck = shieldHit || (target instanceof ShipAPI && NCModPlugin.TemplarsExists && ((ShipAPI) target).getVariant().getHullMods().contains("tem_latticeshield") && TEM_LatticeShield.shieldLevel(((ShipAPI) target)) > 0f);
            if (!shieldHitCheck) {
                dmgType = DamageType.HIGH_EXPLOSIVE;
                //vfx
                Neutrino_ParticlesEffectLib.AddParticles(
                        30, 2, false,
                        point, 5f,
                        facing, 30f,
                        -200, 100,
                        5, 10,
                        0.7f, 1f,
                        0.5f, 2f,
                        color3);
            } else {
                engine.applyDamage(target, point, projectile.getDamageAmount(), DamageType.ENERGY, emp, false, true, projectile.getSource());
                dmgType = DamageType.FRAGMENTATION;
                countMult = 3;
                //vfx
                Neutrino_ParticlesEffectLib.AddParticles(
                        25, 1, false,
                        point, 5f,
                        facing, 30f,
                        -200, -30,
                        5, 10,
                        0.8f, 1f,
                        0.5f, 2f,
                        color3);
                Neutrino_ParticlesEffectLib.AddParticles(
                        12, 1, false,
                        point, 5f,
                        facing, 30f,
                        30, 100,
                        4, 8,
                        0.8f, 1f,
                        0.5f, 1.5f,
                        color4);
                color1 = color2;
            }
            for(int i = 0;i<mult * countMult;i++){
                if (shieldHitCheck && Math.random()< ((ShipAPI)target).getFluxTracker().getFluxLevel() +0.2f){
                    continue;
                }
                engine.spawnEmpArcPierceShields(projectile.getSource(), point, target, target,
                        dmgType,
                        dam,
                        emp,
                        150, // max range 
                        null,
                        5, // thickness
                        color1,
                        color2
                );
        }
//            List<Vector2f> arcTargetList = new ArrayList<>();
//            if (target instanceof ShipAPI) {
//                for (ShipEngineControllerAPI.ShipEngineAPI tpm : ((ShipAPI) target).getEngineController().getShipEngines()) {
//                    arcTargetList.add(tpm.getLocation());
//                    count++;
//                }
//                for (WeaponAPI tpm : ((ShipAPI) target).getAllWeapons()) {
//                    arcTargetList.add(tpm.getLocation());
//                    count++;
//                }
//                for (int i = 0; i < count * countMult;) {
//                    Vector2f random = MathUtils.getRandomPointInCircle(target.getLocation(), target.getCollisionRadius());
//                    if (CollisionUtils.isPointWithinBounds(random, target)) {
//                        i++;
//                        arcTargetList.add(random);
//                    }
//                }
//                Collections.shuffle(arcTargetList);
//                from = point;
//            }
//            for (int i = 0; i <= mult; i++) {
//                if (arcTargetList.isEmpty()) {
//                    engine.spawnEmpArc(projectile.getSource(), point, target, target,
//                            dmgType,
//                            0,
//                            0, // emp 
//                            maxArcRange, // max range 
//                            null,
//                            (mult + 1 - i) * 5f + 10f, // thickness
//                            color1,
//                            color2
//                    );
//                    continue;
//                }
//                for (Vector2f to : arcTargetList) {
//                    if (MathUtils.isWithinRange(from, to, maxArcRange) || (i == 0 && shieldHitCheck)) {
//                        this.to = to;
//                        Collections.shuffle(arcTargetList);
//                        break;
//                    }
//                }
//                if (to == null) {
//                    Vector2f tmp = Vector2f.sub(arcTargetList.get(0), from, null);
//                    tmp.normalise(tmp);
//                    tmp.scale(maxArcRange);
//                    to = Vector2f.add(from, tmp, null);
//                }
//                engine.spawnEmpArc(projectile.getSource(), from, target, new AnchoredEntity(target, to),
//                        dmgType,
//                        0,
//                        0, // emp 
//                        10000, // max range 
//                        null,
//                        (mult + 1 - i) * 5f + 20f, // thickness
//                        color1,
//                        color2
//                );
//                engine.applyDamage(target, to, dam, dmgType, emp, bypassShields, false, projectile.getSource());
//                from = to;
//            }
            critSet.remove(projectile);
        }
    }
}
