// by Deathfly
package data.scripts.weapons;

import com.fs.starfarer.api.combat.BeamAPI;
import com.fs.starfarer.api.combat.BeamEffectPlugin;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.api.util.Misc;
import data.scripts.util.Neutrino_ParticlesEffectLib;
import java.awt.Color;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

public class NeutHERPEffect implements BeamEffectPlugin {

    public NeutHERPEffect() {
    }
    private final IntervalUtil interval = new IntervalUtil(0.09F, 0.11F);
    private final IntervalUtil interval1 = new IntervalUtil(0.09F, 0.11F);
    private final IntervalUtil interval2 = new IntervalUtil(0.11F, 0.13F);
    private final IntervalUtil interval3 = new IntervalUtil(0.13F, 0.15F);
    private final IntervalUtil interval4 = new IntervalUtil(0.15F, 0.17F);
    private final IntervalUtil interval5 = new IntervalUtil(0.17F, 0.19F);

    private int count1 = 20;
    private int count2 = 20;
    private int count3 = 20;
    private int count4 = 20;
    private int count5 = 20;
    private final static Color Color1 = new Color(127, 50, 255, 255);
    private final static Color Color2 = new Color(255, 255, 255, 200);

    @Override
    public void advance(float amount, CombatEngineAPI engine, BeamAPI beam) {
        if (engine.isPaused()) {
            return;
        }
        Vector2f vel = beam.getSource().getVelocity();
        float facing = beam.getWeapon().getCurrAngle();
        float brightness = beam.getBrightness();
        if (brightness >= 0.4) {
            interval.advance(amount);
            if (interval.intervalElapsed()) {
                Neutrino_ParticlesEffectLib.AddParticlesOnSegment(
                        beam.getFrom(),
                        MathUtils.getPointOnCircumference(beam.getFrom(), 300, facing),
                        5 * brightness,
                        1,
                        false,
                        20, 2,
                        brightness * 300, beam.getBrightness() * 750,
                        4, 10,
                        brightness, 1f,
                        0.05f, 0.2f,
                        Misc.interpolateColor(Color1, Color2, brightness));
            }
        }
        if (brightness >= 1) {
            if (count1 > 0) {
                interval1.advance(amount);
                if (interval1.intervalElapsed()) {
                    engine.spawnProjectile(
                            beam.getSource(),
                            beam.getWeapon(),
                            "neutrino_herp1", 
                            beam.getFrom(),
                            MathUtils.clampAngle(facing + MathUtils.getRandomNumberInRange(-5, 5)),
                            vel);
                    count1--;
                }
            }
            if (count2 > 0) {
                interval2.advance(amount);
                if (interval2.intervalElapsed()) {
                    engine.spawnProjectile(
                            beam.getSource(),
                            beam.getWeapon(),
                            "neutrino_herp2",
                            beam.getFrom(),
                            MathUtils.clampAngle(facing + MathUtils.getRandomNumberInRange(-5, 5)),
                            vel);
                    count2--;
                }
            }
            if (count3 > 0) {
                interval3.advance(amount);
                if (interval3.intervalElapsed()) {
                    engine.spawnProjectile(
                            beam.getSource(),
                            beam.getWeapon(),
                            "neutrino_herp3",
                            beam.getFrom(),
                            MathUtils.clampAngle(facing + MathUtils.getRandomNumberInRange(-5, 5)),
                            vel);
                    count3--;
                }
            }
            if (count4 > 0) {
                interval4.advance(amount);
                if (interval4.intervalElapsed()) {
                    engine.spawnProjectile(
                            beam.getSource(),
                            beam.getWeapon(),
                            "neutrino_herp4",
                            beam.getFrom(),
                            MathUtils.clampAngle(facing + MathUtils.getRandomNumberInRange(-5, 5)),
                            vel);
                    count4--;
                }
            }
            if (count5 > 0) {
                interval5.advance(amount);
                if (interval5.intervalElapsed()) {
                    engine.spawnProjectile(
                            beam.getSource(),
                            beam.getWeapon(),
                            "neutrino_herp5",
                            beam.getFrom(),
                            MathUtils.clampAngle(facing + MathUtils.getRandomNumberInRange(-5, 5)),
                            vel);
                    count5--;
                }
            }
        }
    }
}
