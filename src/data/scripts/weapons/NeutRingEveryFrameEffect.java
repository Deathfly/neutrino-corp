package data.scripts.weapons;

import com.fs.starfarer.api.AnimationAPI;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import java.awt.Color;

public class NeutRingEveryFrameEffect implements EveryFrameWeaponEffectPlugin {

    private final float idleFPS = 10f;
    private final float activeFPS = 16f;
    private final float deltaFPS = 2f;

    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
        ShipAPI ship = weapon.getShip();
        AnimationAPI anime = weapon.getAnimation();
        float currentFPS = anime.getFrameRate();
        if (ship.isHulk() && currentFPS > 0) {
            float setFPS = currentFPS - deltaFPS * amount;
            if (setFPS < 0) {
                setFPS = 0;
            }
            anime.setFrameRate(setFPS);
        } else if (ship.getPhaseCloak().isActive() && currentFPS < activeFPS) {
            float setFPS = currentFPS + deltaFPS * amount;
            weapon.getSprite().setColor(new Color(255, 175, 255, 255));
            if (setFPS > activeFPS) {
                setFPS = activeFPS;
            }
            anime.setFrameRate(setFPS);
        } else if (!ship.getPhaseCloak().isCoolingDown() && currentFPS > idleFPS) {
            float setFPS = currentFPS - deltaFPS * amount;
            if (setFPS < idleFPS) {
                setFPS = idleFPS;
            }
            anime.setFrameRate(setFPS);
        }
    }
}
