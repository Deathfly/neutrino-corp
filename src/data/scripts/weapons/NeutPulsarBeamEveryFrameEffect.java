// By Deathfly
package data.scripts.weapons;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponSize;
import java.util.HashMap;
import java.util.Map;

public class NeutPulsarBeamEveryFrameEffect implements EveryFrameWeaponEffectPlugin {

    private static final Map<WeaponSize, Integer> timer = new HashMap<>(3);

    static {
        timer.put(WeaponSize.SMALL, 6);
        timer.put(WeaponSize.MEDIUM, 24);
        timer.put(WeaponSize.LARGE, 3);
    }
    private static final Map<WeaponSize, Float> factor = new HashMap<>(3);

    static {
        factor.put(WeaponSize.SMALL, 1f / (0.8f * 3f));
        factor.put(WeaponSize.MEDIUM, 0.4f / (0.2f * 3f));
        factor.put(WeaponSize.LARGE, 3.5f / (3f * 3f));
    }

//    private static final Map<WeaponSize, Float> extraCooldown = new HashMap<>(3);
//
//    static {
//        factor.put(WeaponSize.SMALL, 2f);
//        factor.put(WeaponSize.MEDIUM, 3f);
//        factor.put(WeaponSize.LARGE, 5f);
//    }   
    private int counter = 0;
    private float cooldownReduce = 0;
    private boolean fired = false;
    private boolean skip = false;

    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
        // refit screen skip.
        if (weapon.getShip().getOriginalOwner() == -1 || weapon.getShip().isHulk()) {
            return;
        }

        if (!weapon.isFiring() && weapon.getChargeLevel() <= 0) {
            if (skip || weapon.getCooldownRemaining() > 0) {
                skip = false;
            } else {
                counter = 0;
                cooldownReduce = 0;
                fired = false;
            }
        } else {
            if (weapon.getChargeLevel() > 0f) {
                skip = true;
                if (!fired && weapon.getCooldownRemaining() > 0) {
                    counter = 1;
                    cooldownReduce = weapon.getCooldown() * ((float) counter / timer.get(weapon.getSize())) * factor.get(weapon.getSize());
                    fired = true;
                } else if (fired && weapon.getCooldownRemaining() != 0 && weapon.getCooldownRemaining() <= cooldownReduce + amount) {
                    weapon.setRemainingCooldownTo(0);
                    if (counter < timer.get(weapon.getSize())) {
                        counter++;
                        cooldownReduce = weapon.getCooldown() * ((float) counter / timer.get(weapon.getSize())) * factor.get(weapon.getSize());
                    }
                }
            }
        }
    }
}
