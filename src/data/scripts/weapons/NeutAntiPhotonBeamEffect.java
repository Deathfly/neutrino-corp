// by Deathfly
package data.scripts.weapons;

import com.fs.starfarer.api.combat.BeamAPI;
import com.fs.starfarer.api.combat.BeamEffectPlugin;
import com.fs.starfarer.api.combat.CollisionClass;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import data.scripts.plugins.Neutrino_LocalData;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;

public class NeutAntiPhotonBeamEffect implements BeamEffectPlugin {

    private final float maxAimOffset = 0.5f;
    private final float trunRate = 1f;
    private final IntervalUtil interval = new IntervalUtil(0.1f, 0.1f);
    private final separatelyAimBeam SAB;
    private static final String KEY = "Neutrino_LocalData";

    public NeutAntiPhotonBeamEffect() {
        interval.forceIntervalElapsed();
        SAB = new separatelyAimBeam(maxAimOffset, trunRate);
    }

    @Override
    public void advance(float amount, CombatEngineAPI engine, BeamAPI beam) {
        if (engine == null) {
            return;
        }
        final Neutrino_LocalData.LocalData localData = (Neutrino_LocalData.LocalData) engine.getCustomData().get(KEY);
        if (engine.isPaused()) {
            return;
        }

        if (beam.getBrightness() > 0) {
            interval.advance(amount);
            if (interval.intervalElapsed()) {
                // tarcking missiles
                WeaponAPI weapon = beam.getWeapon();
                float closestDist = Float.MAX_VALUE;
                float aimOffset = Float.MAX_VALUE;
                MissileAPI target = null;
                for (MissileAPI m : engine.getMissiles()) {
                    if (m.getCollisionClass() == CollisionClass.NONE) {
                        continue;
                    }
                    float dist = MathUtils.getDistance(beam.getFrom(), m.getLocation());
                    if (dist > weapon.getRange()) {
                        continue;
                    }
                    float offset = MathUtils.getShortestRotation(weapon.getCurrAngle(), VectorUtils.getAngle(beam.getFrom(), m.getLocation()));
                    if (Math.abs(offset) > maxAimOffset) {
                        continue;
                    }
//                    if (dist < closestDist) {
//                        closestDist = dist;
//                        aimOffset = offset;
//                        target = m;
//                    }
                    if (Math.abs(offset) < Math.abs(aimOffset)) {
                        closestDist = dist;
                        aimOffset = offset;
                        target = m;
                    }
                }
                if (target == null) {
                    aimOffset = 0;
                }
                SAB.setBeam(beam);
                SAB.setWeapon(weapon);
                SAB.setTarget(target);
                SAB.setTargetAimOffset(aimOffset);
                // OK, we can't modify beam.getTo() in BeamPlugin so we have to pass this Data to somewhere else.
                localData.antiPhotonAimData.put(beam, SAB);
            }
        }
    }

    public class separatelyAimBeam {

        protected Object target;
        protected float turnRate;
        protected float currAimOffset;
        protected float targetAimOffset;
        protected float maxAimOffset;
        protected BeamAPI beam;
        protected WeaponAPI weapon;

        public separatelyAimBeam(float maxAimOffset, float turnRate) {
            this.turnRate = turnRate;
            this.maxAimOffset = maxAimOffset;
            this.currAimOffset = 0;
        }

        public BeamAPI getBeam() {
            return beam;
        }

        public void setBeam(BeamAPI beam) {
            this.beam = beam;
        }

        public float getCurrAimOffset() {
            return currAimOffset;
        }

        public void setCurrAimOffset(float currAimOffset) {
            this.currAimOffset = currAimOffset;
        }

        public float getMaxAimOffset() {
            return maxAimOffset;
        }

        public void setMaxAimOffset(float maxAimOffset) {
            this.maxAimOffset = maxAimOffset;
        }

        public Object getTarget() {
            return target;
        }

        public void setTarget(Object target) {
            this.target = target;
        }

        public float getTargetAimOffset() {
            return targetAimOffset;
        }

        public void setTargetAimOffset(float targetAimOffset) {
            this.targetAimOffset = targetAimOffset;
        }

        public float getTurnRate() {
            return turnRate;
        }

        public void setTurnRate(float turnRate) {
            this.turnRate = turnRate;
        }

        public WeaponAPI getWeapon() {
            return weapon;
        }

        public void setWeapon(WeaponAPI weapon) {
            this.weapon = weapon;
        }

    }
}
