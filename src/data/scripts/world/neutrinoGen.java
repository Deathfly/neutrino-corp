package data.scripts.world;

import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorGeneratorPlugin;
import com.fs.starfarer.api.campaign.RepLevel;

import com.fs.starfarer.api.impl.campaign.shared.SharedData;
import com.fs.starfarer.api.impl.campaign.ids.Factions;

import data.scripts.world.systems.CoronaAustralis;
import data.scripts.world.systems.CorvusAddon;
import data.scripts.world.systems.Exelion;
import java.util.List;

@SuppressWarnings("unchecked")
public class neutrinoGen implements SectorGeneratorPlugin {

    @Override
    public void generate(SectorAPI sector) {

        new CoronaAustralis().generate(sector);
        new Exelion().generate(sector);
        new CorvusAddon().generate(sector);
        //sector.registerPlugin(new CoreCampaignPluginImpl());

        /*
         NCcargo.addCrew(CrewXPLevel.VETERAN, 50);
         NCcargo.addCrew(CrewXPLevel.REGULAR, 100);
         NCcargo.addMarines(200);
         NCcargo.addSupplies(500);
         NCcargo.addFuel(750);

         NCcargo.addWeapons("neutrino_sapper", 6);
         NCcargo.addWeapons("neutrino_advancedtorpedosingle", 6);
         NCcargo.addWeapons("neutrino_disruptor", 8);			
         NCcargo.addWeapons("neutrino_pulsar", 8);		
         //NCcargo.addWeapons("neutrino_photongun", 3);	
         NCcargo.addWeapons("neutrino_dualpulsar", 4);		
         NCcargo.addWeapons("neutrino_advancedtorpedo", 4);			
         NCcargo.addWeapons("neutrino_photontorpedo", 2);
         NCcargo.addWeapons("neutrino_antiproton", 2);
         NCcargo.addWeapons("neutrino_darkmatterbeamcannon", 2);	
         NCcargo.addWeapons("neutrino_derp_launcher", 4);			
         NCcargo.addWeapons("neutrino_pulsebeam", 6);		
         NCcargo.addWeapons("neutrino_neutronpulse", 9);		
         NCcargo.addWeapons("neutrino_misery", 6);			
         NCcargo.addWeapons("neutrino_neutronpulseheavy", 3);		
         NCcargo.addWeapons("neutrino_neutronpulsebattery", 2);			
         NCcargo.addWeapons("neutrino_tractorbeam", 2);		
         //NCcargo.addWeapons("neutrino_heavyphotonrepeater", 2);
         NCcargo.addWeapons("neutrino_dualpulsebeam", 2);		
         NCcargo.addWeapons("neutrino_heavypulsar", 4);				
         NCcargo.addWeapons("neutrino_particlecannonarray", 2);			
         NCcargo.addWeapons("neutrino_XLadvancedtorpedo", 2);
		
         //Frigates
		
         //NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_singularity_standard", null);	//use the _Hull for empty ship.
         NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_singularity_updated", null);		
         NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_singularity_updated", null);	
         //NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_singularity_elite", null);	
         NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_criticality_standard", null);			
         NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_relativity_standard", null);		//use the _Hull for empty ship.
         //NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_relativity_standard", null);		
         NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_causality_standard", null);		//use the _Hull for empty ship.
         NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_polarity_standard", null);		//use the _standard for empty ship.
         NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_polarity_standard", null);		
         NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_polarity_standard", null);
         //NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_polarity_standard", null);
         //NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_polarity_standard", null);
		
         //Destroyers
		
         NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_blowtorch_standard", null);		//use the _Bare for empty ship.
         //use the _Hull for empty ship.
         NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_hacksaw_standard", null);			
         //NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_hacksaw_assault", null);			
         //NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_vice_standard", null);			//use the _Hull for empty ship.
         NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_piledriver_standard", null);		//use the _Bare for empty ship.		
         NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_sledgehammer_standard", null);			//use the _Hull for empty ship.
	
		
         //Cruisers
		
	
         NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_grinder_standard", null);			//use the _Hull for empty ship.	
         //NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_grinder_standard", null);			//use the _Hull for empty ship.			
         NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_nirvash_standard", null);		
         //NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_nirvash_standard", null);		//use the _Bare for empty ship.	
				

         //Capitals
		
         //NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_hammer_standard", null);			
         NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_jackhammer2_standard", null);		//use the _Hull for empty ship.
         //NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_jackhammer_Hull", null);		//use the _Hull for empty ship.		
         //NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_nausicaa2_standard", null);		//use the _Hull for empty ship.
         //NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_hildolfr_standard", null);		//use the _Hull for empty ship.	
         //NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_banshee_standard", null);		//use the _Hull for empty ship.
         //NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_banshee_balanced", null);		//use the _Hull for empty ship.
         //NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_banshee_elite", null);			//use the _Hull for empty ship.		
         NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_colossus_standard", null);		//use the _Hull for empty ship.
         //NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_colossus_standard", null);
         //NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_adventure_standard", null);		//use the _Hull for empty ship.
         //NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_unsung_standard", null);
         //NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_unsung_elite", null);
         //NCcargo.addMothballedShip(FleetMemberType.SHIP, "neutrino_unsung_pirated", null);
		
         //Fighters
		
         //NCcargo.addMothballedShip(FleetMemberType.FIGHTER_WING, "neutrino_floh_wing", null);
         //NCcargo.addMothballedShip(FleetMemberType.FIGHTER_WING, "neutrino_floh_wing", null);		
         //NCcargo.addMothballedShip(FleetMemberType.FIGHTER_WING, "neutrino_floh_wing", null);		
         //NCcargo.addMothballedShip(FleetMemberType.FIGHTER_WING, "neutrino_floh_wing", null);		
         //NCcargo.addMothballedShip(FleetMemberType.FIGHTER_WING, "neutrino_floh_wing", null);		
         //NCcargo.addMothballedShip(FleetMemberType.FIGHTER_WING, "neutrino_floh_wing", null);

         //NCcargo.addMothballedShip(FleetMemberType.FIGHTER_WING, "neutrino_schwarm_wing", null);			
         NCcargo.addMothballedShip(FleetMemberType.FIGHTER_WING, "neutrino_drohne_wing", null);	
         //NCcargo.addMothballedShip(FleetMemberType.FIGHTER_WING, "neutrino_drohne_wing", null);		
         //NCcargo.addMothballedShip(FleetMemberType.FIGHTER_WING, "neutrino_schwarzgeist_wing", null);		
         NCcargo.addMothballedShip(FleetMemberType.FIGHTER_WING, "neutrino_drache_wing", null);		
	
         // Woo thanks Uomoz, hopefully you won't see this, but credits to you. :D
         */
        //system.addSpawnPoint((SpawnPointPlugin) NCSpawn);
        //for (int i = 0; i < 2; i++) NCSpawn.spawnFleet();	
        FactionAPI neutrinocorp = sector.getFaction("neutrinocorp");
        neutrinocorp.setRelationship(Factions.PLAYER, RepLevel.NEUTRAL);

        neutrinocorp.setRelationship(Factions.HEGEMONY, RepLevel.NEUTRAL);
        neutrinocorp.setRelationship(Factions.TRITACHYON, RepLevel.HOSTILE);
        neutrinocorp.setRelationship(Factions.PIRATES, RepLevel.HOSTILE);
        neutrinocorp.setRelationship(Factions.INDEPENDENT, RepLevel.FAVORABLE);
        neutrinocorp.setRelationship(Factions.LUDDIC_CHURCH, RepLevel.HOSTILE);
//        neutrinocorp.setRelationship(Factions.KOL, RepLevel.HOSTILE);
        neutrinocorp.setRelationship(Factions.LUDDIC_PATH, RepLevel.HOSTILE);

        neutrinocorp.setRelationship("lotus_pirates", RepLevel.HOSTILE);
        neutrinocorp.setRelationship("exigency", RepLevel.HOSTILE);
        neutrinocorp.setRelationship("exipirated", RepLevel.HOSTILE);
        neutrinocorp.setRelationship("diableavionics", RepLevel.SUSPICIOUS);
//        List<FactionAPI> factions = sector.getAllFactions();
//        for (FactionAPI faction: factions){
//            if (!faction.isShowInIntelTab()){
//                neutrinocorp.setRelationship(faction.getId(), RepLevel.NEUTRAL);
//            }
//        }

    }
}
