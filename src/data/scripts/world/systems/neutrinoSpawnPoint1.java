package data.scripts.world.systems;

import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FleetAssignment;
import com.fs.starfarer.api.campaign.LocationAPI;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;

import data.scripts.world.BaseSpawnPoint;

public class neutrinoSpawnPoint1 extends BaseSpawnPoint {

	public neutrinoSpawnPoint1(SectorAPI sector, LocationAPI location, 
							float daysInterval, int maxFleets, SectorEntityToken anchor) {
		super(sector, location, daysInterval, maxFleets, anchor);
	}

	@Override
	protected CampaignFleetAPI spawnFleet() {
		//if ((float) Math.random() < 0.5f) return null;
		
		String type = null;
		float r = (float) Math.random();
		if (r > 0.64f) {			//	36% (64)
			type = "neutrinoScout";			
		} else if (r > 0.50f) {		//	14% (50)
			type = "neutrinolightEscort";			
		} else if (r > 0.35f) {		//	15% (35)
			type = "neutrinoEscort";
		} else if (r > 0.20f) {		//	15% (25)
			type = "neutrinopeaceFleet";
		} else if (r > 0.10f) {		//	10% (10)
			type = "neutrinolawyerDetachment";					
		} else  {					//	10%
			type = "neutrinoguardianFleet";
		}  
		
		CampaignFleetAPI fleet = getSector().createFleet("neutrinocorp", type);
		getLocation().spawnFleet(getAnchor(), 0, 0, fleet);
		
		
		if (type.equals("neutrinoscout") || type.equals("neutrinolightEscort") || type.equals("neutrinoEscort") || type.equals("neutrinoeliteEscort") || type.equals("neutrinolawyerDetachment") ||type.equals("neutrinobattleFleet")) {
			fleet.addAssignment(FleetAssignment.RAID_SYSTEM, null, 10);
			fleet.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN, getAnchor(), 1000);
		} else {
			if ((float) Math.random() > 0.95f) {
				fleet.addAssignment(FleetAssignment.RAID_SYSTEM, null, 30);
				fleet.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN, getAnchor(), 1000);
			} else {
				fleet.addAssignment(FleetAssignment.DEFEND_LOCATION, getAnchor(), 20);
				fleet.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN, getAnchor(), 1000);
			}
		}
		
		return fleet;
	}

}
