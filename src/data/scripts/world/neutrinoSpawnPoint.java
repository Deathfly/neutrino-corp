package data.scripts.world;

import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FleetAssignment;
import com.fs.starfarer.api.campaign.LocationAPI;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;

import data.scripts.world.BaseSpawnPoint;

public class neutrinoSpawnPoint extends BaseSpawnPoint {

	public neutrinoSpawnPoint(SectorAPI sector, LocationAPI location, 
							float daysInterval, int maxFleets, SectorEntityToken anchor) {
		super(sector, location, daysInterval, maxFleets, anchor);
	}

		protected CampaignFleetAPI spawnFleet() {
		//if ((float) Math.random() < 0.5f) return null;
		
		String type = null;
		float r = (float) Math.random();
		if (r > 0.64f) {			//	36% (64)
			type = "neutrinoScout";
		} else if (r > 0.60f) {		//	4% (60)
			type = "neutrinoSuspicious";				
		} else if (r > 0.50f) {		//	10% (50)
			type = "neutrinolightEscort";			
		} else if (r > 0.35f) {		//	15% (35)
			type = "neutrinoEscort";
		} else if (r > 0.25f) {		//	10% (25)
			type = "neutrinopeaceFleet";
		} else if (r > 0.20f) {		//	5% 	(20)
			type = "neutrinoeliteEscort";
		} else if (r > 0.10f) {		//	10% (10)
			type = "neutrinolawyerDetachment";		
		} else if (r > 0.05f) {		//	5%  (5)
			type = "neutrinoguardianFleet";				
		} else  {					//	5%
			type = "neutrinobattleFleet";
		}  
		
		CampaignFleetAPI fleet = getSector().createFleet("neutrinocorp", type);
		getLocation().spawnFleet(getAnchor(), 0, 0, fleet);
		
		
		if (type.equals("neutrinoscout") || type.equals("neutrinolightEscort") || type.equals("neutrinoEscort") || type.equals("neutrinoeliteEscort") || type.equals("neutrinolawyerDetachment") ||type.equals("neutrinobattleFleet")) {
			fleet.addAssignment(FleetAssignment.RAID_SYSTEM, null, 10);
			fleet.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN, getAnchor(), 1000);
		} else {
			if ((float) Math.random() > 0.95f) {
				fleet.addAssignment(FleetAssignment.RAID_SYSTEM, null, 30);
				fleet.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN, getAnchor(), 1000);
			} else {
				fleet.addAssignment(FleetAssignment.DEFEND_LOCATION, getAnchor(), 20);
				fleet.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN, getAnchor(), 1000);
			}
		}
		
		return fleet;
	}

}
