package data.scripts.plugins;

import com.fs.starfarer.api.GameState;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.ArmorGridAPI;
import com.fs.starfarer.api.combat.BaseEveryFrameCombatPlugin;
import com.fs.starfarer.api.combat.BeamAPI;
import com.fs.starfarer.api.combat.CollisionClass;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.CombatFleetManagerAPI;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.input.InputEventAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.combat.entities.Ship;
import data.hullmods.NeutrinoNeutroniumPlating.PowerAromr;
import static data.scripts.util.Neutrino_CollisionUtilsEX.getShipCollisionPoint;
import data.scripts.util.Neutrino_ParticlesEffectLib;
import data.scripts.weapons.NeutAntiPhotonBeamEffect;
import data.scripts.weapons.NeutAntiPhotonBeamEffect.separatelyAimBeam;

import java.util.List;
import java.awt.Color;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Map;
import java.util.WeakHashMap;

import org.lwjgl.util.vector.Vector2f;

import org.lazywizard.lazylib.combat.entities.AnchoredEntity;
import org.lazywizard.lazylib.MathUtils;

import org.dark.shaders.distortion.DistortionShader;
import org.dark.shaders.distortion.WaveDistortion;
import org.dark.shaders.light.LightShader;
import org.dark.shaders.light.StandardLight;

public class Neutrino_EveryFrameCombatPlugin extends BaseEveryFrameCombatPlugin {

    private CombatEngineAPI engine;

    private static final String KEY = "Neutrino_LocalData";
    private static boolean paused = false;
    // DATA
    // For phaseMissileEffect
    private static final float phaseFadeTime = 2f;
    private final IntervalUtil flashInterval = new IntervalUtil(0.1F, 0.1F);
    //Projectile List
    private static final String neutrino_phase_missile_ID = "neutrino_phase_missile";
    private static final String neutrino_super_phase_missile_ID = "neutrino_super_phase_missile";
    private static final String neutrino_phase_missile_payload_ID = "neutrino_phase_missile2";
    private static final String neutrino_sapper_missile_ID = "neutrino_sapper_missile";
    private static final String neutrino_heavypulsar_shot_ID = "neutrino_heavypulsar_shot";
    private static final String neutrino_pulsar_shot_ID = "neutrino_pulsar_shot";
    //drone list
    private static final String neutrino_guardianshield_ID = "neutrino_guardianshield";
    private static final String neutrino_aegis_ID = "neutrino_aegis";
    //ship list
    private static final String neutrino_colossus_ID = "neutrino_colossus";
    private static final String neutrino_guardianshield_wing_ID = "neutrino_guardianshield_wing";
    //some maps to optimize CPU loads
    private Map<DamagingProjectileAPI, Vector2f> neutrino_pulsar_shot_relative_velocity;

    @Override
    public void advance(float amount, List<InputEventAPI> events) {
        if (engine == null) {
            return;
        }
        final Neutrino_LocalData.LocalData localData = (Neutrino_LocalData.LocalData) engine.getCustomData().get(KEY);
        // we needs to do some tricks when the engine.isPaused() so make this check        
        if (!engine.isPaused()) {
            paused = false;
            // OK, do some shield drone check first
            List<ShipAPI> ships = engine.getShips();
            List<ShipAPI> toCheck = new ArrayList<>();
            if (!ships.isEmpty()) {
                for (ShipAPI ship : ships) {
                    switch (ship.getHullSpec().getHullId()) {
                        case neutrino_colossus_ID:
                            ShipAPI shieldShip = localData.guardianShieldMap.get(ship);
                            if (shieldShip == null) {
                                CombatFleetManagerAPI FM = engine.getFleetManager(ship.getOwner());
                                FM.setSuppressDeploymentMessages(true);
                                shieldShip = FM.spawnShipOrWing(neutrino_guardianshield_wing_ID, ship.getLocation(), ship.getFacing());
                                shieldShip.setOwner(ship.getOwner());
                                if (ship.isAlly()) {
                                    FM.getDeployedFleetMember(shieldShip).getMember().setAlly(true);
                                }
                                localData.guardianShieldMap.put(ship, shieldShip);
                                FM.setSuppressDeploymentMessages(false);
//                                localData.guardianShieldRadiusMap.put(ship, 360f);
//                                shieldShip.setShipAI(new Neutrino_NoAI());
////                                engine.getFleetManager(ship.getOwner()).removeFromReserves(CombatUtils.getFleetMember(shieldShip));
                            }
                            break;
                        case neutrino_aegis_ID:
                            toCheck.add(ship);
                            if (ship.isHulk() || ship.getShield().isOff()) {
                                ship.setCollisionClass(CollisionClass.FIGHTER);
                                ship.setCollisionRadius(45);
                            } else if (ship.getShield().isOn()) {
                                ship.setCollisionRadius(200);
                            }
                            break;
                        case neutrino_guardianshield_ID:
                            toCheck.add(ship);
                            if (ship.isHulk() || ship.getShield().isOff()) {
                                ship.setCollisionClass(CollisionClass.FIGHTER);
                                ship.setCollisionRadius(45);
//                                localData.guardianShieldRadiusMap.put(ship, 360f);
                            } else if (ship.getShield().isOn()) {
//                                ship.getShield().setActiveArc(360);
//                                float radius = localData.guardianShieldRadiusMap.get(ship);
//                                radius = radius > 1500? 1500:radius + 360f * amount;
                                ship.setCollisionRadius(1500);
                            }
                            if (ship.getFluxTracker().isOverloaded() && Math.random() < 0.1) {
                                engine.applyDamage(ship, ship.getLocation(), MathUtils.getRandomNumberInRange(0F, 100F), DamageType.ENERGY, 0f, true, true, ship);
                                break;
                            }
                    }
                }
            }
            if (!localData.guardianShieldMap.isEmpty()) {
                for (ShipAPI mothership : localData.guardianShieldMap.keySet()) {
                    ShipAPI drone = localData.guardianShieldMap.get(mothership);
                    ShipSystemAPI system = mothership.getSystem();
                    if (mothership.isAlive() && drone.isAlive()) {
                        if (system.isOn()) {
                            drone.getShield().toggleOn();
                        } else {
                            drone.getShield().toggleOff();
                        }
                        drone.setShipTarget(mothership.getShipTarget());
                        drone.getLocation().set(mothership.getLocation());
                        drone.setFacing(mothership.getFacing());
                    } else if (!mothership.isAlive() && drone.isAlive()) {
                        engine.applyDamage(drone, drone.getLocation(), drone.getMaxHitpoints() * 2, DamageType.OTHER, 0, true, true, drone);
                    }
                }
            }
            //shield drone check END

            //Damaging Projectiles check
            List<DamagingProjectileAPI> projectiles = engine.getProjectiles();

            if (!projectiles.isEmpty()) {
                for (DamagingProjectileAPI proj : projectiles) {
                    // make shield drone shield impenetrable
                    if (proj.didDamage()
                            && proj.getCollisionClass() != CollisionClass.NONE
                            && proj.getDamageTarget() instanceof ShipAPI
                            && toCheck.contains(proj.getDamageTarget())) {
                        engine.removeEntity(proj);
                        continue;
                    }
                    // END

                    // Projectile's EveryFrameCombatPlugin  
                    String projSpecId = proj.getProjectileSpecId();
                    flashInterval.advance(amount);
                    if (projSpecId != null) {
                        switch (projSpecId) {
                            case neutrino_phase_missile_ID:
                            case neutrino_phase_missile_payload_ID:
                            case neutrino_super_phase_missile_ID:
                                phaseMissileEffect(proj, amount);
                                break;
                            case neutrino_sapper_missile_ID:
                                Vector2f vel = proj.getVelocity();
                                vel.normalise(vel);
                                float mod = Math.max(150 + proj.getSource().getMutableStats().getMissileMaxSpeedBonus().computeEffective(700f)
                                        - (proj.getSource().getMutableStats().getMissileMaxSpeedBonus().computeEffective(400f) * proj.getElapsed()),
                                        150f);
                                vel.scale(mod);
                                break;
                            case neutrino_heavypulsar_shot_ID:
                            case neutrino_pulsar_shot_ID:
                                if (proj.didDamage()) {
                                    continue;
                                }
                                Vector2f VVel;
                                if (neutrino_pulsar_shot_relative_velocity.containsKey(proj)) {
                                    VVel = neutrino_pulsar_shot_relative_velocity.get(proj);
                                } else {

                                    VVel = MathUtils.getPointOnCircumference(null, 1, proj.getFacing() + 90);
                                    VVel.scale(Vector2f.dot(proj.getVelocity(), VVel));
                                    neutrino_pulsar_shot_relative_velocity.put(proj, VVel);
                                }
                                float speed = proj.getVelocity().length();
                                Vector2f tmp = MathUtils.getPointOnCircumference(proj.getLocation(), speed * amount, MathUtils.clampAngle(proj.getFacing() + 180));
                                int particlesPerFrame = Math.round(200f * amount);
                                for (int i = 0; i < particlesPerFrame; i++) {
                                    Vector2f random = MathUtils.getRandomPointOnLine(tmp, proj.getLocation());
                                    Neutrino_ParticlesEffectLib.AddParticles(
                                            1, 2, true,
                                            random, 5,
                                            proj.getFacing(), 30,
                                            VVel,
                                            0, speed * 0.03f,
                                            5, 8,
                                            0.9f, 1f,
                                            0.3f, 0.5f,
                                            Color.WHITE);
                                }
                                break;
                            default:
                        }
                    }
                    // Projectile's EveryFrameCombatPlugin END
                }
            }
            // make shield drone shield impenetrable, beams part
            List<BeamAPI> beams = engine.getBeams();
            if (!beams.isEmpty()) {
                for (BeamAPI beam : beams) {
                    if (beam.getBrightness() > 0) {
                        Vector2f closest = null;
                        float closestDist = Float.MAX_VALUE;
                        for (ShipAPI shipToCheck : toCheck) {
                            if (beam.getSource().getOwner() == shipToCheck.getOwner()) {
                                continue;
                            }
                            Vector2f tmp = getShipCollisionPoint(beam.getFrom(), beam.getTo(), shipToCheck);
                            if (closest == null) {
                                closest = tmp;
                            } else if (tmp != null && MathUtils.getDistance(tmp, beam.getFrom()) < closestDist) {
                                closest = tmp;
                                closestDist = MathUtils.getDistance(tmp, beam.getFrom());
                            }
                        }
                        if (closest != null) {
                            beam.getTo().set(closest);
                        }
                    }
                }
            }
            // make shield drone shield impenetrable, beams part END

            // anti-photon traking
            for (BeamAPI beam : localData.antiPhotonAimData.keySet()) {                
                separatelyAimBeam SAB = localData.antiPhotonAimData.get(beam);
                float aimOffset = SAB.getCurrAimOffset();
                if (aimOffset == SAB.getTargetAimOffset()) {
                    continue;
                } else {
                    float trun = SAB.getTurnRate() * amount;
                    float targetAimOffset = SAB.getTargetAimOffset();
                    float diff = aimOffset = targetAimOffset;
                    if (Math.abs(diff) < trun) {
                        aimOffset = targetAimOffset;
                    } else {
                        aimOffset += Math.copySign(trun, -diff);
                    }
                    SAB.setCurrAimOffset(aimOffset);
                }
                beam.getTo().set(MathUtils.getPointOnCircumference(beam.getFrom(), MathUtils.getDistance(beam.getFrom(), beam.getTo()), SAB.getWeapon().getCurrAngle() + aimOffset));
            }
            // anti-photon traking. END

            // we needs to make something run once when engine.isPaused()
            // for NeutrinoNeutroniumPlating after battle restroge trick
        } else if (engine.isPaused() && !paused
                && engine.getFleetManager(FleetSide.ENEMY).getTaskManager(false).isInFullRetreat()) {
            if (!localData.powerAromrState.isEmpty()) {
                for (ShipAPI ship : localData.powerAromrState.keySet()) {
                    ArmorGridAPI armorGrid = ship.getArmorGrid();
                    PowerAromr powerAromr = localData.powerAromrState.get(ship);
                    if (!powerAromr.active) {
                        for (int i = 0; i < powerAromr.x; i++) {
                            for (int j = 0; j < powerAromr.y; j++) {
                                powerAromr.armorValueWithoutPlating[i][j] = Math.min(armorGrid.getArmorValue(i, j), powerAromr.maxArmorPerCell - powerAromr.maxPowerArmorPerCell);
                            }
                        }
                    }
                    for (int i = 0; i < powerAromr.x; i++) {
                        for (int j = 0; j < powerAromr.y; j++) {
                            armorGrid.setArmorValue(i, j, Math.min(powerAromr.maxArmorPerCell, Math.max(0, powerAromr.armorValueWithoutPlating[i][j]) + powerAromr.maxPowerArmorPerCell));
                        }
                    }
                    powerAromr.justPaused = true;
                }
            }
            paused = true;
        }
        // NeutrinoNeutroniumPlating after battle restroge trick END

        // something needs to be done all the times...well, maintainStatusForPlayerShip, for player ship only
        ShipAPI playerShip = Global.getCombatEngine().getPlayerShip();
        if (Global.getCurrentState() == GameState.COMBAT && playerShip != null && !playerShip.isHulk()) {
            // Neutronium Plating
            PowerAromr powerAromr = localData.powerAromrState.get(playerShip);
            if (powerAromr != null) {
                if (powerAromr.active) {
                    float armorPercent = (powerAromr.extarArmor / powerAromr.maxExtarArmor) * 100;
                    BigDecimal b = new BigDecimal(armorPercent);
                    String armorPercentS = b.setScale(1, RoundingMode.HALF_UP).toString();
                    if ("100.0".equals(armorPercentS)) {
                        armorPercentS = "100";
                    }
                    String data = "integrity at " + armorPercentS + "%";
                    engine.maintainStatusForPlayerShip("NeutroniumPlatingStatus1", "graphics/neut/icons/hullsys/neutrino_NeutroniumPlating_StatusIcon.png", "Neutronium Plating", data, false);
                    if (playerShip.getFluxTracker().isVenting()) {
                        engine.maintainStatusForPlayerShip("NeutroniumPlatingStatus2", "graphics/neut/icons/hullsys/neutrino_NeutroniumPlating_StatusIcon.png", "Neutronium Plating", "Plating Resistance lowered due to venting", true);
                    }
                } else {
                    float timeRemain = (powerAromr.resetThreshold - powerAromr.extarArmor) / powerAromr.extarArmorRegenPerSec;
                    BigDecimal b = new BigDecimal(timeRemain);
                    String timeRemainS = b.setScale(2, RoundingMode.HALF_UP).toString();
                    String data = "will restore in " + timeRemainS + " seconds";
                    engine.maintainStatusForPlayerShip("NeutroniumPlatingStatus1", "", "Neutronium Plating", data, true);
                    if (playerShip.getFluxTracker().isOverloaded()) {
                        engine.maintainStatusForPlayerShip("NeutroniumPlatingStatus2", "", "Neutronium Plating", "plating collapsed due to overloaded", true);
                    }
                }
            }
            // Neutronium Plating END

//            // Guardian Shield
//            if (localData.guardianShieldMap.containsKey(playerShip)) {
//                ShipAPI drone = localData.guardianShieldMap.get(playerShip);
//                if (drone.isAlive()) {
//                    FluxTrackerAPI shieldFlux = drone.getFluxTracker();
//                    if (shieldFlux.isOverloaded()) {
//                        Global.getCombatEngine().maintainStatusForPlayerShip("GuardianShieldStatus", "graphics/icons/hullsys/fortress_shield.png", "Guardian Shield", "Warning! Shield Core Overloaded!", true);
//                    } else {
//                        String state;
//                        if (playerShip.getSystem().isOn()) {
//                            state = "Actived: Flux Capacity At ";
//                        } else {
//                            state = "Standby: Flux Capacity At ";
//                        }
//                        float fluxLevel = shieldFlux.getFluxLevel();
//                        fluxLevel *= 100f;
//                        BigDecimal b = new BigDecimal(fluxLevel);
//                        String fluxLevelS = b.setScale(2, RoundingMode.HALF_UP).toString();
//                        String data = state + fluxLevelS + "%";
//                        Global.getCombatEngine().maintainStatusForPlayerShip("GuardianShieldStatus", "graphics/icons/hullsys/fortress_shield.png", "Guardian Shield", data, false);
//                    }
//                } else {
//                    Global.getCombatEngine().maintainStatusForPlayerShip("GuardianShieldStatus", "graphics/icons/hullsys/fortress_shield.png", "Guardian Shield", "Warning! Shield Core Ejected!", true);
//                }
//            }
        }
    }

    private void phaseMissileEffect(DamagingProjectileAPI proj, float amount) {
        float elapsed = proj.getElapsed();
        Vector2f loc = proj.getLocation();
        MissileAPI missile = (MissileAPI) proj;
        switch (proj.getProjectileSpecId()) {
            case neutrino_phase_missile_ID:
            case neutrino_super_phase_missile_ID:
                missile.getSpriteAPI().setAlphaMult(Math.min(elapsed * 0.02f, 0.1f));
                float maxFlightTime = missile.getMaxFlightTime();
                if (flashInterval.intervalElapsed()) {
                    Vector2f.add(loc, MathUtils.getRandomPointInCircle(null, 5f), loc);
                }
                if (elapsed < amount) {
                    float dis = proj.getSource().getCollisionRadius() + MathUtils.getRandomNumberInRange(50f, 200f);
                    Vector2f mov = MathUtils.getRandomPointOnCircumference(null, dis);
                    Vector2f.add(loc, mov, loc);
                    WaveDistortion wave = new WaveDistortion();
                    wave.setLocation(loc);
                    wave.setIntensity(25f);
                    wave.setLifetime(0.5f);
                    wave.setSize(15f);
                    wave.fadeOutIntensity(0.25f);
                    DistortionShader.addDistortion(wave);
                    StandardLight light = new StandardLight();
                    light.setLocation(loc);
                    light.setColor(new Color(255, 175, 255, 50));
                    light.setSize(10f);
                    light.setIntensity(1f);
                    light.fadeOut(0.25f);
                    LightShader.addLight(light);
                }
                if (maxFlightTime - elapsed < phaseFadeTime) {
                    missile.getSpriteAPI().setAlphaMult(Math.min((Math.max(maxFlightTime - elapsed, 0f)) / phaseFadeTime * 0.1f, MathUtils.getRandomNumberInRange(0f, 0.1f)));
                }
                break;
            case neutrino_phase_missile_payload_ID:
                if (elapsed < amount) {
                    Vector2f zero = new Vector2f();
                    WaveDistortion wave = new WaveDistortion();
                    wave.setLocation(loc);
                    wave.setIntensity(50f);
                    wave.setLifetime(0.5f);
                    wave.setSize(50f);
                    wave.fadeOutIntensity(0.25f);
                    DistortionShader.addDistortion(wave);
                    StandardLight light = new StandardLight();
                    light.attachTo(proj);
                    light.setLocation(loc);
                    light.setColor(new Color(255, 175, 255, 50));
                    light.setSize(30f);
                    light.setIntensity(0.2f);
                    light.fadeOut(0.3f);
                    LightShader.addLight(light);
                    Global.getSoundPlayer().playSound("system_phase_cloak_collision", 1f, 1f, loc, zero);
                    Global.getSoundPlayer().playSound("neutrino_lightlaunch", 0.5f, 0.5f, loc, zero);
                    Vector2f randomPoint = MathUtils.getRandomPointOnCircumference(loc, 5f);
                    CombatEntityAPI anchor1 = new AnchoredEntity(proj, randomPoint);
                    engine.spawnEmpArc(proj.getSource(), loc, anchor1, anchor1, DamageType.ENERGY, 0f, 0f, 50f, null, 2f,
                            new Color(51, 37, 51, 100), new Color(255, 0, 255, 0));
                }
                if (elapsed <= 1f) {
                    missile.getSpriteAPI().setAlphaMult(elapsed);
                    if (flashInterval.intervalElapsed()) {
                        Vector2f randomPoint = MathUtils.getRandomPointOnCircumference(loc, 5f);
                        CombatEntityAPI anchor1 = new AnchoredEntity(proj, randomPoint);
                        engine.spawnEmpArc(proj.getSource(), loc, anchor1, anchor1, DamageType.ENERGY, 0f, 0f, 50f, null, 2f,
                                new Color(51, 37, 51, 100), new Color(255, 0, 255, 0));
                    }
                }
                break;
        }
    }

    @Override
    public void init(CombatEngineAPI engine) {
        this.engine = engine;
        this.neutrino_pulsar_shot_relative_velocity = new WeakHashMap<>(250);
    }

}
