package data.scripts;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.PluginPick;
import com.fs.starfarer.api.campaign.CampaignPlugin;
import com.fs.starfarer.api.combat.DroneLauncherShipSystemAPI;
import com.fs.starfarer.api.combat.MissileAIPlugin;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAIPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.shared.SharedData;
import data.scripts.AIs.Missiles.Neutrino_AdvancedTorpedoAI;
import data.scripts.AIs.Missiles.Neutrino_JavelinTorpedoAI;
import data.scripts.AIs.Missiles.Neutrino_PhotonTorpedoAI;
import data.scripts.AIs.Missiles.Neutrino_UnstablePhotonAI;
import data.scripts.AIs.Ships.Neutrino_NoAI;
import data.scripts.AIs.Ships.Neutrino_ShieldDroneAI;
import data.scripts.world.neutrinoGen;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.dark.shaders.light.LightData;
import org.dark.shaders.util.ShaderLib;

public class NCModPlugin extends BaseModPlugin {

    private static final String neutrino_advancedtorpedo_ID = "neutrino_advancedtorpedocase";
    private static final String neutrino_photon_torpedo_ID = "neutrino_photon_torpedo";
    private static final String neutrino_lightphoton_torpedo_ID = "neutrino_lightphoton_torpedo";
    private static final String neutrino_javelintorpedo_ID = "neutrino_javelintorpedo";
    private static final String neutrino_unstable_photon_ID = "neutrino_unstable_photon";
    private static final String neutrino_split_photon1_ID = "neutrino_split_photon1";
    private static final String neutrino_split_photon2_ID = "neutrino_split_photon2";
    private static final String neutrino_split_final_ID = "neutrino_split_final";
    private static final String neutrino_guardianshield_ID = "neutrino_guardianshield";

    public static boolean SSPExists = false;
    public static boolean TemplarsExists = false;
    public static boolean ExerlinExists = false;

    @Override
    public void onApplicationLoad() {
        try {
            Global.getSettings().getScriptClassLoader().loadClass("org.dark.shaders.util.ShaderLib");
        } catch (ClassNotFoundException ex) {
            return;
        }
        try {
            Global.getSettings().getScriptClassLoader().loadClass("data.scripts.TEMModPlugin");
            TemplarsExists = true;
        } catch (ClassNotFoundException ex) {
        }
        try {
            Global.getSettings().getScriptClassLoader().loadClass("data.scripts.SSPModPlugin");
            SSPExists = true;
        } catch (ClassNotFoundException ex) {
        }
        try {
            Global.getSettings().getScriptClassLoader().loadClass("exerelin.campaign.SectorManager");
            ExerlinExists = true;
        } catch (ClassNotFoundException ex) {
        }
        ShaderLib.init();
        LightData.readLightDataCSV("data/lights/neutrino_bling.csv");
    }

    @Override
    public PluginPick<MissileAIPlugin> pickMissileAI(MissileAPI missile, ShipAPI launchingShip) {
        switch (missile.getProjectileSpecId()) {
            case neutrino_advancedtorpedo_ID:
                return new PluginPick<MissileAIPlugin>(new Neutrino_AdvancedTorpedoAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
            case neutrino_lightphoton_torpedo_ID:
            case neutrino_photon_torpedo_ID:
                return new PluginPick<MissileAIPlugin>(new Neutrino_PhotonTorpedoAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
            case neutrino_javelintorpedo_ID:
                if (launchingShip != null && launchingShip.getVariant().getHullMods().contains("eccm")) {
                    return new PluginPick<MissileAIPlugin>(new Neutrino_JavelinTorpedoAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
                }
                break;
            case neutrino_unstable_photon_ID:
            case neutrino_split_photon1_ID:
            case neutrino_split_photon2_ID:
            case neutrino_split_final_ID:
                return new PluginPick<MissileAIPlugin>(new Neutrino_UnstablePhotonAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);

            default:
        }
        return null;
    }

    @Override
    public PluginPick<ShipAIPlugin> pickShipAI(FleetMemberAPI member, ShipAPI ship) {
        switch (ship.getHullSpec().getHullId()) {
            case neutrino_guardianshield_ID:
                return new PluginPick<ShipAIPlugin>(new Neutrino_NoAI(), CampaignPlugin.PickPriority.MOD_SPECIFIC);
            default:
        }
        return null;
    }

    @Override
    public PluginPick<ShipAIPlugin> pickDroneAI(ShipAPI drone,
            ShipAPI mothership, DroneLauncherShipSystemAPI system) {
        String id = drone.getHullSpec().getHullId();

        if (id.equals("neutrino_aegis")) {
            return new PluginPick<ShipAIPlugin>(new Neutrino_ShieldDroneAI(drone, mothership, system), CampaignPlugin.PickPriority.MOD_SPECIFIC);
        }
        return null;
    }

    @Override
    public void onNewGame() {
        try {
            //Got Exerelin, so load Exerelin  
            Class<?> def = Global.getSettings().getScriptClassLoader().loadClass("exerelin.campaign.SectorManager");
            Method method;
            try {
                method = def.getMethod("getCorvusMode");
                Object result = method.invoke(def);
                if ((boolean) result == true) {
                    // Exerelin running in Corvus mode, go ahead and generate our sector  
                    new neutrinoGen().generate(Global.getSector());
                }
            } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException |
                    InvocationTargetException ex) {
                // check failed, do nothing  
            }
        } catch (ClassNotFoundException ex) {
            new neutrinoGen().generate(Global.getSector());
            // Exerelin not found so continue and run normal generation code
        }
        SharedData.getData().getPersonBountyEventData().addParticipatingFaction("neutrinocorp");
    }
}
